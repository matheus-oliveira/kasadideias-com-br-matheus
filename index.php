<?php include_once('./env.php'); ?>

<?php include_once('./view/layout/header.php'); ?>

<!-- Style page quimera -->
<link rel="stylesheet" href="./css/style.css">

<?php include_once('./view/layout/menu.php'); ?>

<section id="empusa" class="empusa bg-empusa">
    <h1 class="title__dark">Você tem idéais inovadoras <br> mas não sabe como executá-las?</h1>
    <p class="sub-title__dark">Venha saber como podemos <span class="font-weight-bold text-uppercase">fazer</span> pra
        você!</p>
    <div class="cta-link-promo-white">
        <!-- <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a> -->
        <!-- <a href="#"> Agende uma reunião <i class="fas fa-angle-right"></i></a> -->
    </div>
</section>

<section id="quimera" class="quimera bg-quimera">
    <h1 class="title__dark">Transforme sua ideia <br> em um produto de sucesso</h1>
    <p class="sub-title__dark">Sua marca não tem que ser vista, precisa ser lembrada</p>
    <div class="cta-link">
        <!-- <a href="./view/quimera.html"> Agende já <i class="fas fa-angle-right"></i></a> -->
        <!-- <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a> -->
    </div>
</section>

<section id="fobos" class="fobos">
    <div class="container-fluid">
        <div class="row">
            <div class="promo start bg-start col-md-6 col-lg-6">
                <h1 class="title__dark-promo">Start</h1>
                <p class="sub-title__dark-promo">Você tem ideias inovadoras <br>
                    mas não sabe como desenvolvê-las?<br>
                    Saiba o que podemos fazer <span class="font-weight-bold">POR VOCÊ!</span>
                </p>
                <div class="cta-link-promo">
                    <a class="btn btn-light shadow rounded-pill" href="./view/info_start.php">
                        Saiba mais <i class="fas fa-angle-right"></i></a>
                    <a class="btn btn-primary shadow rounded-pill" href="./view/start.php">
                        Comprar <i class="fas fa-angle-right"></i></a>
                </div>
            </div>
            <div class="promo marketing bg-marketing col-md-6 col-lg-6">
                <h1 class="title-promo">Marketing</h1>
                <p class="sub-title-promo">Saiba o que é relevante e acompanhe o desempenho real das atividades de
                    marketing</p>
                <div class="cta-link-promo">
                    <a class="btn btn-light shadow rounded-pill" href="./view/info_marketing.php">
                        Saiba mais <i class="fas fa-angle-right"></i></a>
                    <a class="btn btn-primary shadow rounded-pill" href="./view/marketing.php">
                        Comprar <i class="fas fa-angle-right"></i></a>
                </div>
            </div>
            <div class="promo negocio bg-negocio col-md-6 col-lg-6">
                <h1 class="title__dark-promo">Negócio</h1>
                <p class="sub-title__dark-promo">Antes de ser empresa, sou cliente</p>
                <div class="cta-link-promo">
                    <a class="btn btn-light shadow rounded-pill" href="./view/info_negocio.php">
                        Saiba mais <i class="fas fa-angle-right"></i></a>
                    <a class="btn btn-primary shadow rounded-pill" href="./view/negocio.php">
                        Comprar <i class="fas fa-angle-right"></i></a>
                </div>
            </div>
            <div class="promo arquitetura bg-arquitetura col-md-6 col-lg-6">
                <h1 class="title__dark-promo">Arquitetura</h1>
                <p class="sub-title__dark-promo">Antes de ser empresa, sou cliente</p>
                <div class="cta-link-promo-white">
                    <!-- <a class="btn btn-primary rounded-pill" href="#"> Saiba mais <i class="fas fa-angle-right"></i></a> -->
                    <a class="btn btn-primary shadow rounded-pill" href="./view/arquitetura.php"> Comprar <i
                            class="fas fa-angle-right"></i></a>
                </div>
            </div>
            <div class="promo prototipo bg-prototipo col-md-6 col-lg-6">
                <h1 class="title-promo">Protótipo</h1>
                <br> <br>
                <!-- <p class="sub-title-promo">Lorem ipsum dolor sit amet</p> -->
                <div class="cta-link-promo">
                    <!-- <a class="btn btn-primary rounded-pill" href="./view/info_prototipo.php"> Saiba mais <i
                            class="fas fa-angle-right"></i></a> -->
                    <a class="btn btn-primary shadow rounded-pill" href="./view/prototipo.php">
                        Comprar <i class="fas fa-angle-right"></i></a>
                </div>
            </div>
            <div class="promo database bg-database col-md-6 col-lg-6">
                <h1 class="title__dark-promo">Data & IA</h1>
                <p class="sub-title__dark-promo">
                    Leve o poder dos Dados e da Inteligência Artificial para o seu negócio
                </p>
                <div class="cta-link-promo-white">
                    <!-- <a class="btn btn-primary shadow rounded-pill" href="./view/info_database.php"> Saiba mais
                        <i class="fas fa-angle-right"></i></a> -->
                    <a class="btn btn-primary shadow rounded-pill" href="./view/database.php">
                        Comprar <i class="fas fa-angle-right"></i></a>
                </div>
            </div>
            <div class="promo juridico bg-juridico col-md-6 col-lg-6">
                <h1 class="title-promo">Jurídico</h1>
                <p class="sub-title-promo">Iniciando a sua empresa e <br> protegendo seu negócio
                </p>
                <div class="cta-link-promo">
                    <a class="btn btn-primary rounded-pill" href="./view/info_juridico.php">
                        Saiba mais <i class="fas fa-angle-right"></i></a>
                    <a class="btn btn-primary shadow rounded-pill" href="./view/juridico.php">
                        Comprar <i class="fas fa-angle-right"></i></a>
                </div>
            </div>
            <div class="promo criar bg-criar col-md-6 col-lg-6">
                <h1 class="title__dark-promo">Criação</h1>
                <br> <br>
                <!-- <p class="sub-title-promo">Lorem ipsum dolor sit amet</p> -->
                <div class="cta-link-promo">
                    <!-- <a class="btn btn-primary rounded-pill" href="./view/info_criar.php"> Saiba mais <i
                            class="fas fa-angle-right"></i></a> -->
                    <a class="btn btn-primary shadow rounded-pill" href="./view/criar.php">
                        Comprar <i class="fas fa-angle-right"></i></a>
                </div>
            </div>
            <div class="promo investimento bg-investimento col-md-6 col-lg-6">
                <h1 class="title__dark-promo">Investimento</h1>
                <br> <br>
                <!-- <p class="sub-title__dark-promo">Lorem ipsum dolor sit amet</p> -->
                <div class="cta-link-promo">
                    <a class="btn btn-primary rounded-pill" href="./view/info_investimento.php"> Saiba mais <i
                            class="fas fa-angle-right"></i></a>
                    <a class="btn btn-primary shadow rounded-pill" href="./view/investimento.php"> Comprar <i
                            class="fas fa-angle-right"></i></a>
                </div>
            </div>
            <div class="promo vendas bg-vendas col-md-6 col-lg-6">
                <h1 class="title-promo">Vendas</h1>
                <br> <br>
                <!-- <p class="sub-title-promo">Lorem ipsum dolor sit amet</p> -->
                <div class="cta-link-promo">
                    <!-- <a class="btn btn-primary rounded-pill" href="./view/info_vendas.php">
                        Saiba mais <i class="fas fa-angle-right"></i></a> -->
                    <a class="btn btn-primary shadow rounded-pill" href="./view/vendas.php">
                        Comprar <i class="fas fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include_once('./view/layout/footer.php'); ?>