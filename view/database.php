<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>
<!-- <meta http-equiv="refresh" content="5"> -->

<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Data & IA </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div> -->
        </div>
    </div>
</section>

<section id="planos">
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 ">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p> Criação do modelo de banco de dados Básico, com até 10 tabelas</p>
                        <p> Entrega do <i>script</i> de criação do banco de dados</p>
                        <p> Vídeo da apresentação do modelo de banco de dados</p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_DATABASE_BASICO ?></p>
                        <?php
                        if ($PARCELA_DATABASE_BASICO !== 'X' && $PARCELA_DATABASE_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_DATABASE_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/database-basico/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p>Criação do modelo relacional completo;</p>
                        <p>Criação de dicionário de dados;</p>
                        <p>Disponibilização do ambiente de banco de dados em produção - Banco de dados em nuvem</p>
                        <p>Levantamento de KPI's do produto – Relatório de KPI's​</p>
                        <p>Implementação e disponibilização de <i>BI</i> para até dois KPI’s levantados</p>
                        <p>Vídeo da apresentação do modelo de banco de dados ​</p>
                        <p>Vídeo da apresentação do acesso ao banco de dados ​</p>
                        <p>Vídeo da apresentação do ambiente de <i>BI</i></p>

                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_DATABASE_PROFISSIONAL ?></p>
                        <?php
                        if ($PARCELA_DATABASE_PROFISSIONAL !== 'X' && $PARCELA_DATABASE_PROFISSIONAL !== null) {
                            echo    "<p>Em até " . $PARCELA_DATABASE_PROFISSIONAL . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/database-profissional/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <p class="badge badge-warning text-center"> Todo o pacote profissional</p>
                        <p>Aplicação de funcionalidades de Ciência de dados (Análise Descritiva)​</p>
                        <p>Análise de dados em tempo real</p>
                        <p>Estudo para análise de aplicação de <i>Machine Learning</i></p>
                        <p>*Criação de projeto de <i>Machine Learning</i></p>
                        <p>**Vídeo da apresentação do projeto de <i>Machine Learning</i></p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Os serviços estão sujeitos a análise do uso real das técnicas ao negócio.</p>
                        <p class="small">*A quantidade de dados podem influênciar no resultado final</p>
                        <p class="small">**Reunião com duração de até 4 horas</p>

                        <p class="h3"><?php echo  $PRECO_DATABASE_PREMIUM ?></p>
                        <?php
                        if ($PARCELA_DATABASE_PREMIUM !== 'X' && $PARCELA_DATABASE_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_DATABASE_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/database-premium/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="mensal" class="col">
                <div class="card border-right border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class=" fa-4x fas fa-calendar-week"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Mensal </p>
                        <br>
                        <p> Manutenção de performance do banco de dados </p>
                        <p> Manutenção nas funcionalidades criadas no banco de dados </p>
                        <p> Manutenção nos KPIs desenvolvidos </p>
                        <p> Manutenção nas projeções e resultados no aprendizado de máquina </p>

                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_DATABASE_MENSAL ?></p>
                        <?php
                        if ($PARCELA_DATABASE_PREMIUM !== 'X' && $PARCELA_DATABASE_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_DATABASE_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="#" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>