a<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>
<!-- <meta http-equiv="refresh" content="5"> -->

<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Vendas </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div> -->
        </div>
    </div>
</section>

<section id="planos">
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 ">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p> Diagnóstico de Vendas </p>
                        <p> Formação de Preços para produtos/serviços </p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_VENDAS_BASICO ?></p>
                        <?php
                            if ($PARCELA_VENDAS_BASICO !== 'X' && $PARCELA_VENDAS_BASICO !== null) {
                                echo    "<p>Em até " . $PARCELA_VENDAS_BASICO . "x sem juros
                                        <br> <a href='#'> Saiba mais</a>
                                        </p>";
                            } else {
                                echo    "<br>";
                            }
                            ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/vendas-basico/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p> Planejamento de vendas com previsão de receita </p>
                        <p> Controle de Vendas diárias e cálculo de ticket Médio  ​ </p>
                        <p> Criação de indicadores de vendas</p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_VENDAS_PROFISSIONAL ?></p>
                        <?php
                            if ($PARCELA_VENDAS_PROFISSIONAL !== 'X' && $PARCELA_VENDAS_PROFISSIONAL !== null) {
                                echo    "<p>Em até " . $PARCELA_VENDAS_PROFISSIONAL . "x sem juros
                                        <br> <a href='#'> Saiba mais</a>
                                        </p>";
                            } else {
                                echo    "<br>";
                            }
                            ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/vendas-profissional/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <!-- <p class="badge badge-info text-warning"> Todo o pacote profissional</p> -->
                        <br>
                        <p> Implantação de um modelo de vendas ​ </p>
                        <p> Método de treinamento de vendas personalizado ​ </p>
                        <p> Metodologia de Prospecção de Clientes ​ </p>
                        <p> Implantação de um modelo de Pós Venda ​ </p>
                        <p> CRM​ </p>
                        <p> *Coaching de vendas com 1 colaborador, Metodologia Brian Tracy </p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Treinamento online + Certificado</p>
                        <p class="h3"><?php echo  $PRECO_VENDAS_PREMIUM ?></p>
                        <?php
                            if ($PARCELA_VENDAS_PREMIUM !== 'X' && $PARCELA_VENDAS_PREMIUM !== null) {
                                echo    "<p>Em até " . $PARCELA_VENDAS_PREMIUM . "x sem juros
                                        <br> <a href='#'> Saiba mais</a>
                                        </p>";
                            } else {
                                echo    "<br>";
                            }
                            ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/vendas-premium/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>