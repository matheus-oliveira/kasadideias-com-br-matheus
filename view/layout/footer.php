<!-- Footer -->
<footer class="page-footer font-small text-white bg-dark pt-2 pb-2">
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2020 Copyright:
        <a class="text-white" href="https://kasadideias.com.br/_index.php"> kasadideias.com.br</a>
    </div>
    <!-- Copyright -->
</footer>
<!-- Footer -->


<!-- Optional JavaScript -->
<script src="../../js/app.min.js"> </script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../../js/jquery-3.4.1.slim.min.js"> </script>

<script src="../../js/popper.min.js"> </script>
<script src="../../js/bootstrap.min.js"> </script>

</body>

</html>