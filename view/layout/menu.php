<style>
    .bg-dark {
        background-color: #000000d7 !important;
        -webkit-backdrop-filter: saturate(180%) blur(20px);
        backdrop-filter: saturate(180%) blur(20px);
    }

    .navbar-toggler {
        padding: 0;
        border: 0px solid transparent;
        border-radius: 0;
    }

    .transparent {
        color: transparent !important;
    }

    header {
        position: relative;
        z-index: 1000;
    }
</style>

</head>

<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Logo versão mobile -->
            <a class="navbar-brand d-lg-none" href="../_index.php">
                <img src="../img/logo/logo-dark.webp" width="34" height="34" class="d-inline-block align-top"
                    alt="logo, casa com lampada dentro">
            </a>
            <!-- bag versão mobile -->
            <a class="navbar-brand d-lg-none" href="../view/base.php">
                <i class="fas fa-shopping-bag text-white-50 transparent"></i>
            </a>
            <div class="navbar-collapse justify-content-center collapse" id="navbarsExample08">
                <ul class="navbar-nav text-center" data-offset="0">
                    <li class="nav-item ">
                        <a class="navbar-brand mr-3 d-none d-lg-block" href="../_index.php">
                            <img src="../img/logo/logo-dark.webp" width="30" height="30" alt="logo, casa om lampada dentro">
                        </a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/info_start.php">Start</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/info_marketing.php">Marketing</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/info_negocio.php">Negócio</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/arquitetura.php">Arquitetura</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/prototipo.php">Protótipo</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/database.php">Data & IA</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/info_juridico.php">JurÍdico</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/criar.php">Criação</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/info_investimento.php">Investimento</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="../view/vendas.php">Vendas</a>
                    </li>
                    <!-- <li class="nav-item mr-3 scroll">
                    <a class="nav-link" href="#fobos">Fobos</a>
                </li> -->
                    <li class="nav-item">
                        <a class="navbar-brand pr-2 d-lg-block " href="../view/base.php">
                            <i class="fas fa-shopping-bag text-white-50 transparent"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

    </header>