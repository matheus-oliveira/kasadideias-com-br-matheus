<!doctype html>
<html lang="pt-br">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Dosis:wght@200;300;400;500;523;600;700;800&display=swap"
        rel="stylesheet">

    <!-- Font Icons -->
    <script src="https://kit.fontawesome.com/10f8833c99.js" crossorigin="anonymous"></script>


    <!-- Favicon -->
    <link rel="shortcut icon" href="../icon-dark.ico" type="image/x-icon">
    <title><?php echo  $APP_NAME ?></title>
