<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>


<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Investimento</p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll"
                    href="#pacotes">Planos</a></div>
        </div>
    </div>
</section>
<section class="d-none">
    <div class="container-fluid bg-light ">
        <div class="row">
            <p class="  mt-4 mb-4 offset-2 col-8 text-center font-weight-bolder">Lorem Ipsum is simply dummy text of the
                printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since
                the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-0 offset-md-1 offset-lg-1  col-md-5 col-lg-4 col-xl-4">
                <span class="text-warning font-weight-bold h5">Básico</span>
                <p class="h4">Se sua Startup já possui o MVP agora é hora realmente testar o mercado, com o plano
                    profissional realizaremos o planejamento necessários para o início de suas atividades em grande
                    escala.
                </p>
                <!-- <p class="h2">Potência portátil.</p> -->
                <p><a class="btn btn-primary rounded-pill scroll mt-4" href="#pacotes">Ver preços</a></p>
                <!-- <p> <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a></p> -->
                <!-- <p class="h5 text-gray">Confira em breve a disponibilidade</p> -->
            </div>
            <div class=" col-md-5 col-lg-7 col-xl-7 p-0">
                <img class="w-100" src="../img/planos/investimento/investimento-basico-large.webp" alt="">
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-0 offset-md-1 offset-lg-1  col-md-5 col-lg-5 col-xl-5 p-0">
                <img class="w-100" src="../img/planos/investimento/investimento-profissional-large.webp" alt="">
            </div>
            <div class=" col-md-5 col-lg-6 col-xl-5">
                <span class="text-warning font-weight-bold h5">Profissional</span>
                <p class="h4">Se sua Startup já possui o MVP agora é hora realmente testar o mercado, com o plano
                    profissional realizaremos o planejamento necessários para o início de suas atividades em grande
                    escala.
                </p>
                <!-- <p class="h2">O melhor. <br> Para os melhores.</p> -->
                <p><a class="btn btn-primary rounded-pill scroll mt-4" href="#pacotes">Ver preços</a></p>
                <!-- <p> <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a></p> -->
                <!-- <p class="h5 text-gray">Confira em breve a disponibilidade</p> -->
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-0 offset-md-1 offset-lg-1  col-md-5 col-lg-4 col-xl-5">
                <span class="text-warning font-weight-bold h5">Premium</span>
                <p class="h4">Sua Startup já está a pleno vapor, negócios, clientes e uma boa parcela de sucesso, agora
                    é a hora de expandir! Iremos preparar sua Startup para demonstrar o seu desempenho estar apta para
                    atrair maiores investimentos.</p>
                <!-- <p class="h2">Tão leve que voa. </p> -->
                <p><a class="btn btn-primary rounded-pill scroll mt-4" href="#pacotes">Ver preços</a></p>
                <!-- <p> <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a></p> -->
            </div>
            <div class=" col-md-5 col-lg-7 col-xl-6  bg-mac-16 p-0">
                <img class="w-100" src="../img/planos/investimento/investimento-premium-large.webp" alt="">
            </div>
        </div>
    </div>
</section>

<section id="pacotes" class="bg-light">
    <h1 class="title">Qual é o pacote <span class="font-weight-bolder">IDEAL</span><br> para você?</h1>
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 mt-5 pt-5">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p> Plano de investimento Anjo com <i>smartmoney</i> </p>
                        <p> Plano de investimento Semente com <i>smartmoney</i> </p>
                        <p> <i>Elevator Pitch</i> para investidor</p>
                    </div>
                    <div class="card-footer">
                        <br>
                        <a href="./vendas.php" class="btn btn-primary btn-sm btn-block">Ver preço</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p> Plano de investimento Anjo com <i>smartmoney</i> </p>
                        <p> Plano de investimento Semente com <i>smartmoney</i> </p>
                        <p> Plano de investimento Anjo com Cotista Simples </p>
                        <p> Plano de investimento Semente com Cotista Simples </p>
                        <p> Plano de investimento Semente  com retorno financeiro</p>
                        <p> <i>Elevator Pitch</i> para investidor</p>
                        <p> Criação de roteiro para gravação de<i> Elevator Pitch</i> ​ </p>
                        <p> *Reunião de apresentação do roteiro para gravação de<i> Elevator Pitch</i></p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Reunião Online (1 hora) </p>
                        <br>
                        <a href="./vendas.php" class="btn btn-primary btn-sm btn-block">Ver preço</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <p class="badge badge-warning text-center"> Todo o pacote profissional</p>
                        <br>
                        <p> Gravação dirigida de <i>Elevator Pitch</i> voltado para apresentação para investimento</p>
                        <p> Plano de investimento Serie A</p>
                        <p> Preparação de documentação para migração para Sociedade Anônima </p>
                    </div>
                    <div class="card-footer">
                        <br>
                        <a href="./vendas.php" class="btn btn-primary btn-sm btn-block">Ver preço</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once('./layout/footer.php'); ?>