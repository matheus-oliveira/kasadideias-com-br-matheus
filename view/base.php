<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>
<!-- <meta http-equiv="refresh" content="5"> -->

<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">
<!-- <link rel="stylesheet" href="../css/style_haha.css"> -->
<style>
    .text-gray {
        color: gray !important;
    }
</style>
<?php include_once('./layout/menu.php'); ?>




<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Base </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div> -->
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-0 offset-md-1 offset-lg-1  col-md-5 col-lg-4 col-xl-3">
                <span class="text-warning font-weight-bold h5">Básico</span>
                <p class="h4">Modelo de 13 polegadas <br><span class="font-weight-bold h1">MacBook Pro</span></p>
                <p class="h2">Potência portátil.</p>
                <p><a class="btn btn-primary rounded-pill scroll mt-4" href="#planos">Ver preços</a></p>
                <p> <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a></p>
                <p class="h5 text-gray">Confira em breve a disponibilidade</p>
            </div>
            <div class=" col-md-5 col-lg-7 col-xl-7 p-0">
                <img class="w-100" src="https://www.apple.com/v/mac/home/ap/images/overview/hero/macbook_pro_13__ep96x983izyq_large.webp" alt="">
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-0 offset-md-1 offset-lg-1  col-md-5 col-lg-5 col-xl-5 p-0">
                <img class="w-100" src="https://www.apple.com/v/mac/home/ap/images/overview/hero/macbook_pro_16__ni9nkbyq2dm6_large.webp" alt="">
            </div>
            <div class=" col-md-5 col-lg-6 col-xl-5">
                <span class="text-warning font-weight-bold h5">Profissional</span>
                <p class="h4">Modelo de 16 polegadas <br><span class="font-weight-bold h1">MacBook Pro</span></p>
                <p class="h2">O melhor. <br> Para os melhores.</p>
                <p><a class="btn btn-primary rounded-pill scroll mt-4" href="#planos">Ver preços</a></p>
                <p> <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a></p>
                <!-- <p class="h5 text-gray">Confira em breve a disponibilidade</p> -->
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-0 offset-md-1 offset-lg-1  col-md-5 col-lg-4 col-xl-3">
                <span class="text-warning font-weight-bold h5">Premium</span>
                <p class="h4"><span class="font-weight-bold h1">MacBook Air</span></p>
                <p class="h2">Tão leve que voa. </p>
                <p><a class="btn btn-primary rounded-pill scroll mt-4" href="#planos">Ver preços</a></p>
                <p> <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a></p>
            </div>
            <div class=" col-md-5 col-lg-7 col-xl-8  bg-mac-16 p-0">
                <img class="w-100" src="https://www.apple.com/v/mac/home/ap/images/overview/hero/macbook_air__csdfieli984m_large.webp" alt="">
            </div>
        </div>
    </div>
</section>

<section id="planos">
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 ">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div id="basico" class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <!-- <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p> -->
                    </div>
                    <div class="card-footer">
                        <p class="small">*</p>
                        <p class="small">**</p>
                        <p class="small">***</p>
                        <p class="h3"><?php echo  $PRECO_YYY_BASICO ?></p>
                        <?php
                        if ($PARCELA_YYY_BASICO !== 'X' && $PARCELA_YYY_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_YYY_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="#" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <!-- <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p> -->
                    </div>
                    <div class="card-footer">
                        <p class="small">*</p>
                        <p class="small">**</p>
                        <p class="small">***</p>
                        <p class="h3"><?php echo  $PRECO_YYY_PROFISSIONAL ?></p>
                        <?php
                        if ($PARCELA_YYY_PROFISSIONAL !== 'X' && $PARCELA_YYY_PROFISSIONAL !== null) {
                            echo    "<p>Em até " . $PARCELA_YYY_PROFISSIONAL . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="#" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <!-- <p class="badge badge-info text-warning"> Todo o pacote profissional</p> -->
                        <br>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <!-- <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p> -->
                    </div>
                    <div class="card-footer">
                        <p class="small">*</p>
                        <p class="small">**</p>
                        <p class="small">***</p>
                        <p class="h3"><?php echo  $PRECO_YYY_PREMIUM ?></p>
                        <?php
                        if ($PARCELA_YYY_PREMIUM !== 'X' && $PARCELA_YYY_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_YYY_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="#" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="mensal" class="col">
                <div class="card border-right border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class=" fa-4x fas fa-calendar-week"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Mensal </p>
                        <br>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <!-- <p> ... </p>
                            <p> ... </p>
                        <p> ... </p>
                        <p> ... </p>
                        <p> ... </p> -->

                    </div>
                    <div class="card-footer">
                        <p class="small">*</p>
                        <p class="small">**</p>
                        <p class="small">***</p>
                        <p class="h3"><?php echo  $PRECO_YYY_MENSAL ?></p>
                        <?php
                        if ($PARCELA_YYY_PREMIUM !== 'X' && $PARCELA_YYY_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_YYY_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="#" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>