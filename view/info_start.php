<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>


<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Start </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll"
                    href="#pacotes">Pacotes</a></div>
        </div>
    </div>

    <section>
        <div class="container-fluid bg-light ">
            <div class="row">
                <p class="  mt-4 mb-4 offset-2 col-8 text-center font-weight-bolder">Toda grande ideia precisa ser
                    organizada e planejada, o Plano Start foi pensado para apresentar dados, relatórios e um diagnóstico
                    ideal para iniciar sua trajetória rumo ao sucesso.</p>
            </div>
        </div>
    </section>

</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-0 offset-md-1 offset-lg-1  col-md-5 col-lg-4 col-xl-4">
                <span class="text-warning font-weight-bold h5">Básico</span>
                <p class="h4"> O pacote Básico traz um ótimo custo pra ajudar quem precisa iniciar
                    mas precisa conter os gastos iniciais.
                </p>
                <!-- <p class="h2">Potência portátil.</p> -->
                <p><a class="btn btn-primary rounded-pill scroll mt-4" href="#pacotes">Ver preços</a></p>
                <!-- <p> <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a></p> -->
                <!-- <p class="h5 text-gray">Confira em breve a disponibilidade</p> -->
            </div>
            <div class=" col-md-5 col-lg-7 col-xl-7 p-0">
                <img class="w-100"
                    src="../img/planos/start/start-basico-large.webp"
                    alt="">
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-0 offset-md-1 offset-lg-1  col-md-5 col-lg-5 col-xl-5 p-0">
                <img class="w-100"
                    src="../img/planos/start/start-profissional-large.webp"
                    alt="">
            </div>
            <div class=" col-md-5 col-lg-6 col-xl-5">
                <span class="text-warning font-weight-bold h5">Profissional</span>
                <p class="h4">O pacote Profissional é focado em entregar um produto ou serviço
                    plenamente comercial, um ótimo custo benefício para os que procuram acelerar suas ideias sem avançar
                    muito nos custos.
                </p>
                <!-- <p class="h2">O melhor. <br> Para os melhores.</p> -->
                <p><a class="btn btn-primary rounded-pill scroll mt-4" href="#pacotes">Ver preços</a></p>
                <!-- <p> <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a></p> -->
                <!-- <p class="h5 text-gray">Confira em breve a disponibilidade</p> -->
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-0 offset-md-1 offset-lg-1  col-md-5 col-lg-4 col-xl-5">
                <span class="text-warning font-weight-bold h5">Premium</span>
                <p class="h4">O pacote Premium te entregará um produto completo, com todas as
                    métricas e metodologias de criação de produtos, te guiando e te entregando valores por um preço
                    fixo, permitindo controle dos gastos mesmo com um maior investimento.
                </p>
                <!-- <p class="h2">Tão leve que voa. </p> -->
                <p><a class="btn btn-primary rounded-pill scroll mt-4" href="#pacotes">Ver preços</a></p>
                <!-- <p> <a href="#"> Saiba mais <i class="fas fa-angle-right"></i></a></p> -->
            </div>
            <div class=" col-md-5 col-lg-7 col-xl-6  bg-mac-16 p-0">
                <img class="w-100"
                    src="../img/planos/start/start-premium-large.webp"
                    alt="">
            </div>
        </div>
    </div>
</section>

<section id="pacotes" class="bg-light">
    <h1 class="title">Qual é o pacote <span class="font-weight-bolder">IDEAL</span><br> para você?</h1>
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 mt-5 pt-5">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="bg-transparent card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p>Estudo de mercado para a solução</p>
                        <p>Pesquisa de interesse</p>
                        <p>Teste de um concorrente que mais se assemelhe ao aplicativo/sistema proposto</p>
                    </div>
                    <div class="card-footer">
                        <br>
                        <a href="./start.php" class="btn btn-primary btn-sm btn-block">Ver preço</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="bg-transparent card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p>Estudo de Mercado com gráficos</p>
                        <p>Pesquisa de interesse </p>
                        <p> Teste de 3 concorrentes que mais se assemelhem ao aplicativo/sistema proposto
                            ​</p>
                        <p> Geração de 2 sugestões de propostas de valor</p>
                        <p> Aplicação da metodologia de <i>Growth Hacking</i>​</p>
                    </div>
                    <div class="card-footer">
                        <br>
                        <a href="./start.php" class="btn btn-primary btn-sm btn-block">Ver preço</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="bg-transparent card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <br>
                        <p>Estudo de Mercado com gráficos</p>
                        <p>Pesquisa de interesse</p>
                        <p>Teste de 3 concorrentes que mais se assemelhem ao aplicativo/sistema proposto</p>
                        <p>Geração de 2 sugestões de propostas de valor + 2 Missões +2 Visões</p>
                        <p>Realização de pesquisa de amostragem com 150 pessoas respondendo sobre a proposta de valor
                        </p>
                        <p>Aplicação da metodologia de <i>Growth Hacking</i></p>
                        <p>Relatório de consultoria  conclusiva</p>
                    </div>
                    <div class="card-footer">
                        <br>
                        <a href="./start.php" class="btn btn-primary btn-sm btn-block">Ver preço</a>
                    </div>
                </div>
            </div>

            <div id="mensal" class="col">
                <div class="bg-transparent card border-right border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class=" fa-4x fas fa-calendar-week"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Mensal </p>
                        <br>
                        <p>Relatório mensal de atualização de mercado e concorrentes com apontamento
                            de oportunidades​
                        </p>
                        <p>Reunião de atualização de mercado, proposta e valor, publico alvo, concorrentes
                            e oportunidade
                            <span class="font-italic font-weight-bold">-  Reunião online (1 hora)</span>
                            ​​</p>
                    </div>
                    <div class="card-footer">
                        <br>
                        <a href="./start.php" class="btn btn-primary btn-sm btn-block">Ver preço</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once('./layout/footer.php'); ?>