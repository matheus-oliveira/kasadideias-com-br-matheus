<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>
<!-- <meta http-equiv="refresh" content="5"> -->

<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Protótipo </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div> -->
        </div>
    </div>
</section>

<section id="planos">
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 ">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p> *Prototipação funcional de alta fidelidade de até oito telas, do sistema </p>
                        <p> Uma revisão pós entrega – Correções ou alterações</p>
                        <p> Entrega do Código pronto</p>
                        <p> **15 dias de publicação do protótipo na internet</p>
                        <p> Vídeo da apresentação do protótipo</p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Formato APP ou WEBPAGE</p>
                        <p class="small">**Acesso a um link específico na internet para acesso</p>
                        <p class="h3"><?php echo  $PRECO_PROTOTIPO_BASICO ?></p>
                        <?php
                        if ($PARCELA_PROTOTIPO_BASICO !== 'X' && $PARCELA_PROTOTIPO_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_PROTOTIPO_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/prototipo-basico/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p> Prototipação funcional de até 15 telas do sistema </p>
                        <p> Até 3 revisões de entregas – Correções ou alterações no código </p>
                        <p> Entrega do Código HTML pronto </p>
                        <p> *60 dias de publicação do protótipo na internet </p>
                        <p> Vídeo da apresentação do protótipo </p>
                        <p> **1 Reunião online para demonstração do protótipo e discursão sobre o produto  </p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Acesso a um link específico na internet para acesso   ​ </p>
                        <p class="small">**Reunião com duração de 1 hora</p>
                        <p class="h3"><?php echo  $PRECO_PROTOTIPO_PROFISSIONAL ?></p>
                        <?php
                        if ($PARCELA_PROTOTIPO_PROFISSIONAL !== 'X' && $PARCELA_PROTOTIPO_PROFISSIONAL !== null) {
                            echo    "<p>Em até " . $PARCELA_PROTOTIPO_PROFISSIONAL . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/prototipo-profissional/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <br>
                        <p> *Reunião inicial para alinhamento de requisitos </p>
                        <p> **Reunião para levantamento de requisitos</p>
                        <p> Prototipação funcional de telas Ilimitadas </p>
                        <p> Revisões e entregas a cada 3 dias – Correções ou alterações no código </p>
                        <p> Reunião semanal de acompanhamento do desenvolvimento do produto
                        </p>
                        <p> Entrega do Código HTML pronto </p>
                        <p> Documentação completa do código HTML – Documento de especificação de requisitos
                            do protótipo  HTML.​ </p>
                        <p> 120 dias de publicação do protótipo na internet </p>
                        <p> Vídeo da apresentação do protótipo </p>
                        <p> *1 Reunião online para demonstração do protótipo finalizado e discursão sobre
                            o produto </p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Reunião com duração de 1 hora</p>
                        <p class="small">**Reunião com duração de até 4 hora</p>
                        <p class="small">***Reunião com duração de 30 minutos</p>
                        <p class="small">****Acesso a um link específico na internet para acesso ​ </p>
                        <p class="small">*</p>
                        <p class="h3"><?php echo  $PRECO_PROTOTIPO_PREMIUM ?></p>
                        <?php
                        if ($PARCELA_PROTOTIPO_PREMIUM !== 'X' && $PARCELA_PROTOTIPO_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_PROTOTIPO_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/prototipo-premium/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>