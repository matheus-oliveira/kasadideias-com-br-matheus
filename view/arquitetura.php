<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>
<!-- <meta http-equiv="refresh" content="5"> -->

<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Arquitetura </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div> -->
        </div>
    </div>
</section>

<section id="planos">
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 ">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p>*Reunião <i>on-line</i> para Elicitação e levantamento de requisitos </p>
                        <p>Criação do documento de requisitos funcionais </p>
                        <p> Criação do documento de requisitos não funcionais </p>
                        <p> Diagrama de caso de uso </p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*2 horas reunião <i>on-line</i></p>
                        <p class="h3"><?php echo  $PRECO_ARQUITETURA_BASICO ?></p>
                        <?php
                        if ($PARCELA_ARQUITETURA_BASICO !== 'X' && $PARCELA_ARQUITETURA_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_ARQUITETURA_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/arquitetura-basico/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p> *Levantamento e Mapeamento de Processos
                        </p>
                        <p> Criação do modelo de processo mapeado  ​ </p>
                        <p> **Elicitação e levantamento de requisitos</p>
                        <p>Criação do documento de requisitos funcionais </p>
                        <p> Criação do documento de requisitos não funcionais </p>
                        <p> Diagrama de caso de uso ​ </p>
                        <p> Diagrama de Classe  </p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Limitado a 3 processos - 4 horas reunião <i>on-line</i>​</p>
                        <p class="small">**2 horas reunião <i>on-line</i></p>
                        <p class="h3"><?php echo  $PRECO_ARQUITETURA_PROFISSIONAL ?></p>
                        <?php
                        if ($PARCELA_ARQUITETURA_PROFISSIONAL !== 'X' && $PARCELA_ARQUITETURA_PROFISSIONAL !== null) {
                            echo    "<p>Em até " . $PARCELA_ARQUITETURA_PROFISSIONAL . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/arquitetura-profissional/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <p class="badge badge-warning text-center"> Todo o pacote profissional</p>
                        <p> Engenharia de Requisitos</p>
                        <p> Validação de Requisitos</p>
                        <p> Metodologia <i>crazyeigth</i> para telas estáticas</p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_ARQUITETURA_PREMIUM ?></p>
                        <?php
                        if ($PARCELA_ARQUITETURA_PREMIUM !== 'X' && $PARCELA_ARQUITETURA_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_ARQUITETURA_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/arquitetura-premium/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>