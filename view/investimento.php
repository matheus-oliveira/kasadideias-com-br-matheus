<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>
<!-- <meta http-equiv="refresh" content="5"> -->

<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Investimento </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div> -->
        </div>
    </div>
</section>

<section id="planos">
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 ">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p> Plano de investimento Anjo com <i>smartmoney</i> </p>
                        <p> Plano de investimento Semente com <i>smartmoney</i> </p>
                        <p> <i>Elevator Pitch</i> para investidor</p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_INVESTIMENTO_BASICO ?></p>
                        <?php
                        if ($PARCELA_INVESTIMENTO_BASICO !== 'X' && $PARCELA_INVESTIMENTO_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_INVESTIMENTO_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/investimento-basico/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p> Plano de investimento Anjo com <i>smartmoney</i> </p>
                        <p> Plano de investimento Semente com <i>smartmoney</i> </p>
                        <p> Plano de investimento Anjo com Cotista Simples </p>
                        <p> Plano de investimento Semente com Cotista Simples </p>
                        <p> Plano de investimento Semente  com retorno financeiro</p>
                        <p> <i>Elevator Pitch</i> para investidor</p>
                        <p> Criação de roteiro para gravação de<i> Elevator Pitch</i> ​ </p>
                        <p> *Reunião de apresentação do roteiro para gravação de<i> Elevator Pitch</i></p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Reunião Online (1 hora) </p>
                        <p class="h3"><?php echo  $PRECO_INVESTIMENTO_PROFISSIONAL ?></p>
                        <?php
                        if ($PARCELA_INVESTIMENTO_PROFISSIONAL !== 'X' && $PARCELA_INVESTIMENTO_PROFISSIONAL !== null) {
                            echo    "<p>Em até " . $PARCELA_INVESTIMENTO_PROFISSIONAL . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/investimento-profissional/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <p class="badge badge-warning text-center"> Todo o pacote profissional</p>
                        <br>
                        <p> Gravação dirigida de <i>Elevator Pitch</i> voltado para apresentação para investimento</p>
                        <p> Plano de investimento Serie A</p>
                        <p> Preparação de documentação para migração para Sociedade Anônima </p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_INVESTIMENTO_PREMIUM ?></p>
                        <?php
                        if ($PARCELA_INVESTIMENTO_PREMIUM !== 'X' && $PARCELA_INVESTIMENTO_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_INVESTIMENTO_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/investimento-premium/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>