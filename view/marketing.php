<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>
<!-- <meta http-equiv="refresh" content="5"> -->

<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Marketing </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div> -->
        </div>
    </div>
</section>

<section id="planos">
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 ">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p> *Reunião <i>brainstorm</i> da empresa</p>
                        <p> **2 sugestões de nome com domínio reservado</p>
                        <p> 2 sugestões de Logo​ </p>
                        <p> 1 logotipo completo​ </p>
                        <p> ***1 vídeo explicando a criação do nome e do logo</p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Online por 1 hora </p>
                        <p class="small">** Não incluso pagamento do domínio</p>
                        <p class="small">*** 30 segundos</p>
                        <p class="h3"><?php echo  $PRECO_MARKETING_BASICO ?></p>
                        <?php
                        if ($PARCELA_MARKETING_BASICO !== 'X' && $PARCELA_MARKETING_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_MARKETING_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/marketing-basico/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p> *1 Reunião <i>brainstorm</i> da empresa</p>
                        <p> **3 sugestões de nome com domínio reservado</p>
                        <p> 3 sugestões de Logo​ </p>
                        <p> 1 Logotipo completa​ </p>
                        <p>Manual de identidade visual - MIV</p>
                        <p> Tipografia</p>
                        <p class="font-italic"> Pattern</p>
                        <p> ***Design Gráfico</p>
                        <p> 1 Cartão de Visita digital​ </p>
                        <p> Criação de um <span class="font-italic">Web Site - Single Page Aplication​</span> </p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Online por 1 hora </p>
                        <p class="small">** Não incluso pagamento do domínio</p>
                        <p class="small">*** 1 <span class="font-italic">Folder</span> de 2 dobras​</p>
                        <p class="h3"><?php echo  $PRECO_MARKETING_PROFISSIONAL ?></p>
                        <?php
                        if ($PARCELA_MARKETING_PROFISSIONAL !== 'X' && $PARCELA_MARKETING_PROFISSIONAL !== null) {
                            echo    "<p>Em até " . $PARCELA_MARKETING_PROFISSIONAL . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/marketing-profissional/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <p class="badge badge-warning text-center"> Todo o pacote profissional</p>
                        <p> 2 Cartões de Visita digitais​ </p>
                        <p> Criação de um <span class="font-italic">Web Site - Single Page Aplication​</span> </p>
                        <p> Criação de Blog para geração de conteúdo​ </p>
                        <p> Ativação de <span class="font-italic">gateway</span> de pagamento​ </p>
                        <p> Ativação das redes sociais – Instagram/Facebook​ </p>
                        <p> Ativação do <span class="font-italic">Google Analiticys​</span> </p>
                        <p> Ativação do acompanhamento Hotjar​ </p>
                        <p> Ativação da primeira campanha de e-mail <span class="font-italic">marketing </span> (Mailchimp) </p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_MARKETING_PREMIUM ?></p>
                        <?php
                        if ($PARCELA_MARKETING_PREMIUM !== 'X' && $PARCELA_MARKETING_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_MARKETING_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/marketing-premium/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="mensal" class="col">
                <div class="card border-right border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class=" fa-4x fas fa-calendar-week"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Mensal </p>
                        <br>
                        <p> 2 posts semanais para o público facebook​ </p>
                        <p> 2 posts semanais voltados para o público Instagram​ </p>
                        <p> Ativação e acompanhamento <span class="font-italic">Adwords​</span> </p>
                        <p> Acompanhamento Hotjar​ </p>
                        <p> Acompanhamento com relatórios semanais do <span class="font-italic">Google Analiticys​</span>  </p>
                        <p> 1 landing page com teste A/B mensal para promoções específicas​ </p>
                        <p> *Geração de conteúdo</p>
                        <p> **Reunião mensal de alinhamento de <span class="font-italic">marketing</span> </p>
                        <p> 3 materiais para <span class="font-italic">marketing OutBound</span>  ou eventos pontuais​ </p>
                    </div>
                    <div class="card-footer">
                        <p class="small">*Um artigo quinzenal para o Blog com o mínimo de 500 palavras</p>
                        <p class="small">**Online 90 minutos</p>
                        <p class="h3"><?php echo  $PRECO_MARKETING_MENSAL ?></p>
                        <?php
                        if ($PARCELA_MARKETING_BASICO !== 'X' && $PARCELA_MARKETING_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_MARKETING_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/marketing-mensal/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>