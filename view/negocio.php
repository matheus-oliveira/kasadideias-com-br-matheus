<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>
<!-- <meta http-equiv="refresh" content="5"> -->

<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Negócio </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div> -->
        </div>
    </div>
</section>

<section>
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p> <i>Business Plan</i> – CANVAS <i>(Business Canvas Model)</i> ​ </p>
                        <p> *Proposta de Valor​ </p>
                        <p> Definição do Escopo, Mapa de Valor e Perfil </p>
                        <p> Vídeo sobre a importância do <i>Valuation</i> - Vídeo </p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_NEGOCIO_BASICO ?></p>
                        <?php
                        if ($PARCELA_NEGOCIO_BASICO !== 'X' && $PARCELA_NEGOCIO_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_NEGOCIO_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/negocio-basico/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p> <i>Business Plan</i> – Modelo CANVAS <i>(Business Canvas Model)</i> ​ </p>
                        <p> <i>Business Plan</i> - Análise de Necessidade e Justificativa​ </p>
                        <p> *Proposta de Valor​ </p>
                        <p> Definição do Escopo, Mapa de Valor e Perfil </p>
                        <p> <i>Valuation</i> - Criação/Análise de DRE (Demonstrativo do Resultado de Exercício)
                            – PLANILHA​
                        </p>
                        <p> <i>Valuation</i> - Criação/Análise de Cenários​ </p>
                        <p> Fluxos de Caixa (Análise e Projeção, Custo de Oportunidade e TMA) </p>
                        <p> Definição de Objetivos e Cronogramas </p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_NEGOCIO_PROFISSIONAL ?></p>
                        <?php
                        if ($PARCELA_NEGOCIO_PROFISSIONAL !== 'X' && $PARCELA_NEGOCIO_PROFISSIONAL !== null) {
                            echo    "<p>Em até " . $PARCELA_NEGOCIO_PROFISSIONAL . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/negocio-profissional/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <p class="badge badge-warning text-center"> Todo o pacote profissional</p>
                        <p> Análise de Ganho de Escala e de Risco (Matriz SWOT)​ </p>
                        <p> Estudo de Viabilidade Técnica e Financeira (TIR, VPL, <i>Payback</i> )</p>
                        <p> Metas e Projeções Futuras​ </p>
                        <p> Análise de Resultados e de Maturidade da Empresa </p>
                        <p> <i>Valuation</i> - Cálculo de <i>Valuation</i>​ </p>
                        <p> Taxa de Desconto e Cálculo para Alavancagem</p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_NEGOCIO_PREMIUM ?></p>
                        <?php
                        if ($PARCELA_NEGOCIO_PREMIUM !== 'X' && $PARCELA_NEGOCIO_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_NEGOCIO_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/negocio-premium/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>