<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>


<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Start </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div>
        </div>
    </div>
</section>

<section id="planos">
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 ">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p>Estudo de mercado para a solução</p>
                        <p>Pesquisa de interesse</p>
                        <p>Teste de um concorrente que mais se assemelhe ao aplicativo/sistema proposto</p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_START_BASICO ?></p>
                        <?php
                        if ($PARCELA_START_BASICO !== 'X' && $PARCELA_START_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_START_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/start-basico/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <br>
                        <p>Estudo de Mercado com gráficos</p>
                        <p>Pesquisa de interesse </p>
                        <p> Teste de 3 concorrentes que mais se assemelhem ao aplicativo/sistema proposto
                            ​</p>
                        <p> Geração de 2 sugestões de propostas de valor</p>
                        <p> Aplicação da metodologia de <i>Growth Hacking</i>​</p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_START_PROFISSIONAL ?></p>
                        <?php
                        if ($PARCELA_START_PROFISSIONAL !== 'X' && $PARCELA_START_PROFISSIONAL !== null) {
                            echo    "<p>Em até " . $PARCELA_START_PROFISSIONAL . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/start-profissional/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <br>
                        <p>Estudo de Mercado com gráficos</p>
                        <p>Pesquisa de interesse</p>
                        <p>Teste de 3 concorrentes que mais se assemelhem ao aplicativo/sistema proposto</p>
                        <p>Geração de 2 sugestões de propostas de valor + 2 Missões +2 Visões</p>
                        <p>Realização de pesquisa de amostragem com 150 pessoas respondendo sobre a proposta de valor
                        </p>
                        <p>Aplicação da metodologia de <i>Growth Hacking</i></p>
                        <p>Relatório de consultoria  conclusiva</p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_START_PREMIUM ?></p>
                        <?php
                        if ($PARCELA_START_PREMIUM !== 'X' && $PARCELA_START_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_START_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/start-premium/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="mensal" class="col">
                <div class="card border-right border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class=" fa-4x fas fa-calendar-week"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Mensal </p>
                        <br>
                        <p>Relatório mensal de atualização de mercado e concorrentes com apontamento
                            de oportunidades​
                        </p>
                        <p>Reunião de atualização de mercado, proposta e valor, publico alvo, concorrentes
                            e oportunidade
                            <span class="font-italic font-weight-bold">-  Reunião online (1 hora)</span>
                            ​​</p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_START_MENSAL ?></p>
                        <?php
                        if ($PARCELA_START_PREMIUM !== 'X' && $PARCELA_START_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_START_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/start-mensal/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>