<?php include_once('../env.php'); ?>
<?php include_once('./layout/header.php'); ?>
<!-- <meta http-equiv="refresh" content="5"> -->

<!-- Style page marketing -->
<link rel="stylesheet" href="../css/style_info.min.css">

<?php include_once('./layout/menu.php'); ?>

<section>
    <div class="container border-bottom">
        <div class="d-flex bd-highlight mb-1">
            <div class="mr-auto p-2 bd-highlight">
                <p class="h5 font-weight-bold">Jurídico </p>
            </div>
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Preços</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"><a class="card-link" href="#">Especificações</a></div> -->
            <!-- <div class="p-2 bd-highlight"> <a class="btn btn-primary btn-sm rounded-pill scroll" href="#planos">Comprar</a></div> -->
        </div>
    </div>
</section>

<section id="planos">
    <div class="container-fluid offset-1 col-10 offset-lg-2 col-lg-8 ">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div id="basico" class="col">
                <div class="bg-transparent card border-right h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-box"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Básico </p>
                        <br>
                        <p> Acordo de Confidencialidade <i>(Non disclosure agreement – NDA)</i> – Entrega de documento
                            personalizado​ </p>
                        <p> Memorando de entendimentos <i>(Memorandum of understandment)</i> – Orientações para elaboração
                            e Relatório​ </p>
                        <p> Pesquisa de viabilidade para registro da Marca  – Orientações para elaboração e Relatório
                        </p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_JURIDICO_BASICO ?></p>
                        <?php
                        if ($PARCELA_JURIDICO_BASICO !== 'X' && $PARCELA_JURIDICO_BASICO !== null) {
                            echo    "<p>Em até " . $PARCELA_JURIDICO_BASICO . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/juridico-basico/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="profissional" class="col">
                <div class="bg-transparent card border-right  border-right-md h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-address-card"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Profissional </p>
                        <p class="badge badge-info text-center"> Todo o pacote Básico</p>
                        <p> Contrato de prestação de serviços (colaborador) – Orientações para elaboração e Relatório​
                        </p>
                        <p> Contrato de prestação de serviços (cliente) – Orientações para elaboração e Relatório </p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_JURIDICO_PROFISSIONAL ?></p>
                        <?php
                        if ($PARCELA_JURIDICO_PROFISSIONAL !== 'X' && $PARCELA_JURIDICO_PROFISSIONAL !== null) {
                            echo    "<p>Em até " . $PARCELA_JURIDICO_PROFISSIONAL . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/juridico-profissional/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>

            <div id="premium" class="col">
                <div class="bg-transparent card border-right border-right-0-lg h-100">
                    <div class="card-header-pills mx-auto">
                        <i class="mx-auto fa-4x fas fa-crown"></i>
                    </div>
                    <div class="card-body">
                        <p class="h4">Premium </p>
                        <p class="badge badge-warning text-center"> Todo o pacote profissional</p>
                        <p> Estudo sobre a Lei de Proteção de Dados aplicada ao negócio - Orientações e Relatório​ </p>
                        <p> Relatório final com 2 reuniões para acompanhamento </p>
                    </div>
                    <div class="card-footer">
                        <p class="h3"><?php echo  $PRECO_JURIDICO_PREMIUM ?></p>
                        <?php
                        if ($PARCELA_JURIDICO_PREMIUM !== 'X' && $PARCELA_JURIDICO_PREMIUM !== null) {
                            echo    "<p>Em até " . $PARCELA_JURIDICO_PREMIUM . "x sem juros
                                    <br> <a href='#'> Saiba mais</a>
                                    </p>";
                        } else {
                            echo    "<br>";
                        }
                        ?>
                        <a href="https://kasadideias2.lojavirtualnuvem.com.br/produtos/juridico-premium/" class="btn btn-primary btn-sm btn-block">Comprar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include_once('./layout/footer.php'); ?>