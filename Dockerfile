FROM debian

RUN apt update
RUN apt upgrade -y
RUN apt install apache2 -y
RUN apt install php -y
RUN apt install nano -y
RUN apt install net-tools -y
RUN apt-get install webp -y

RUN rm /var/www/html/index.html
EXPOSE 80

WORKDIR /var/www/html

# COPY .  /var/www/html

#  CMD service apache2 start
   
# docker run --rm -dt --name web -u www-data:www-data -p 80:80 -v D:\\Users\\Oliveira\\'OneDrive - Kasa Didéias'\\Kasadideias\\new_kasadideias.com.br:/var/www/html web:v1  bash

# docker exec -u root -it web service apache2 start