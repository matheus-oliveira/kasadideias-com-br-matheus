<?php
$APP_NAME                         = 'Kasadideias';

$PRECO_START_BASICO               = 'R$ 129,00';    // valor do plano
$PARCELA_START_BASICO             = 'X';            // dividedo em X vezes
$PRECO_START_PROFISSIONAL         = 'R$ 299,00';    // valor do plano
$PARCELA_START_PROFISSIONAL       = 'X';            // dividedo em X vezes
$PRECO_START_PREMIUM              = 'R$ 599,00';    // valor do plano
$PARCELA_START_PREMIUM            = 'X';            // dividedo em X vezes
$PRECO_START_MENSAL               = 'R$ 59,00';     // valor do plano
$PARCELA_START_MENSAL             = 'X';            // dividedo em X vezes

$PRECO_MARKETING_BASICO           = 'R$ 159,00';    // valor do plano
$PARCELA_MARKETING_BASICO         = 'X';            // dividedo em X vezes
$PRECO_MARKETING_PROFISSIONAL     = 'R$ 899,00';    // valor do plano
$PARCELA_MARKETING_PROFISSIONAL   = 'X';            // dividedo em X vezes
$PRECO_MARKETING_PREMIUM          = 'R$ 2.999,00';  // valor do plano
$PARCELA_MARKETING_PREMIUM        = 'X';            // dividedo em X vezes
$PRECO_MARKETING_MENSAL           = 'R$ 899,00';    // valor do plano
$PARCELA_MARKETING_MENSAL         = 'X';            // dividedo em X vezes

$PRECO_NEGOCIO_BASICO             = 'R$ 159,00';    // valor do plano
$PARCELA_NEGOCIO_BASICO           = 'X';            // dividedo em X vezes
$PRECO_NEGOCIO_PROFISSIONAL       = 'R$ 499,00';    // valor do plano
$PARCELA_NEGOCIO_PROFISSIONAL     = 'X';            // dividedo em X vezes
$PRECO_NEGOCIO_PREMIUM            = 'R$ 2.999,00';  // valor do plano
$PARCELA_NEGOCIO_PREMIUM          = 'X';            // dividedo em X vezes
$PRECO_NEGOCIO_MENSAL             = 'R$ XXX,XX';    // valor do plano
$PARCELA_NEGOCIO_MENSAL           = 'X';            // dividedo em X vezes

$PRECO_ARQUITETURA_BASICO         = 'R$ 149,00';    // valor do plano
$PARCELA_ARQUITETURA_BASICO       = 'X';            // dividedo em X vezes
$PRECO_ARQUITETURA_PROFISSIONAL   = 'R$ 699,00';    // valor do plano
$PARCELA_ARQUITETURA_PROFISSIONAL = 'X';            // dividedo em X vezes
$PRECO_ARQUITETURA_PREMIUM        = 'R$ 999,00';    // valor do plano
$PARCELA_ARQUITETURA_PREMIUM      = 'X';            // dividedo em X vezes
$PRECO_ARQUITETURA_MENSAL         = 'R$ XXX,XX';    // valor do plano
$PARCELA_ARQUITETURA_MENSAL       = 'X';            // dividedo em X vezes

$PRECO_PROTOTIPO_BASICO           = 'R$ 199,00';    // valor do plano
$PARCELA_PROTOTIPO_BASICO         = 'X';            // dividedo em X vezes
$PRECO_PROTOTIPO_PROFISSIONAL     = 'R$ 899,00';    // valor do plano
$PARCELA_PROTOTIPO_PROFISSIONAL   = 'X';            // dividedo em X vezes
$PRECO_PROTOTIPO_PREMIUM          = 'R$ 2.999,00';  // valor do plano
$PARCELA_PROTOTIPO_PREMIUM        = 'X';            // dividedo em X vezes
$PRECO_PROTOTIPO_MENSAL           = 'R$ XXX,XX';    // valor do plano
$PARCELA_PROTOTIPO_MENSAL         = 'X';            // dividedo em X vezes

$PRECO_DATABASE_BASICO            = 'R$ 199,00';    // valor do plano
$PARCELA_DATABASE_BASICO          = 'X';            // dividedo em X vezes
$PRECO_DATABASE_PROFISSIONAL      = 'R$ 999,00';    // valor do plano
$PARCELA_DATABASE_PROFISSIONAL    = 'X';            // dividedo em X vezes
$PRECO_DATABASE_PREMIUM           = 'R$ 2.999,00';  // valor do plano
$PARCELA_DATABASE_PREMIUM         = 'X';            // dividedo em X vezes
$PRECO_DATABASE_MENSAL            = 'R$ 699,00';    // valor do plano
$PARCELA_DATABASE_MENSAL          = 'X';            // dividedo em X vezes

$PRECO_JURIDICO_BASICO            = 'R$ 159,00';    // valor do plano
$PARCELA_JURIDICO_BASICO          = 'X';            // dividedo em X vezes
$PRECO_JURIDICO_PROFISSIONAL      = 'R$ 1.099,00';    // valor do plano
$PARCELA_JURIDICO_PROFISSIONAL    = 'X';            // dividedo em X vezes
$PRECO_JURIDICO_PREMIUM           = 'R$ 2.299,00';  // valor do plano
$PARCELA_JURIDICO_PREMIUM         = 'X';            // dividedo em X vezes
$PRECO_JURIDICO_MENSAL            = 'R$ XXX,XX';    // valor do plano
$PARCELA_JURIDICO_MENSAL          = 'X';            // dividedo em X vezes

$PRECO_CRIAR_BASICO               = 'R$ 499,00';    // valor do plano
$PARCELA_CRIAR_BASICO             = 'X';            // dividedo em X vezes
$PRECO_CRIAR_PROFISSIONAL         = 'R$ 5.999,00';  // valor do plano
$PARCELA_CRIAR_PROFISSIONAL       = 'X';            // dividedo em X vezes
$PRECO_CRIAR_PREMIUM              = 'R$ 35.000,00';     // valor do plano
$PARCELA_CRIAR_PREMIUM            = 'X';            // dividedo em X vezes
$PRECO_CRIAR_MENSAL               = 'R$ XXX,XX';    // valor do plano
$PARCELA_CRIAR_MENSAL             = 'X';            // dividedo em X vezes

$PRECO_INVESTIMENTO_BASICO        = 'R$ 189,00';    // valor do plano
$PARCELA_INVESTIMENTO_BASICO      = 'X';            // dividedo em X vezes
$PRECO_INVESTIMENTO_PROFISSIONAL  = 'R$ 899,00';    // valor do plano
$PARCELA_INVESTIMENTO_PROFISSIONAL= 'X';            // dividedo em X vezes
$PRECO_INVESTIMENTO_PREMIUM       = 'R$ 2.999,00';  // valor do plano
$PARCELA_INVESTIMENTO_PREMIUM     = 'X';            // dividedo em X vezes
$PRECO_INVESTIMENTO_MENSAL        = 'R$ XXX,XX';    // valor do plano
$PARCELA_INVESTIMENTO_MENSAL      = 'X';            // dividedo em X vezes

$PRECO_VENDAS_BASICO              = 'R$ 159,00';    // valor do plano
$PARCELA_VENDAS_BASICO            = 'X';            // dividedo em X vezes
$PRECO_VENDAS_PROFISSIONAL        = 'R$ 899,00';    // valor do plano
$PARCELA_VENDAS_PROFISSIONAL      = 'X';            // dividedo em X vezes
$PRECO_VENDAS_PREMIUM             = 'R$ 2.999,00';  // valor do plano
$PARCELA_VENDAS_PREMIUM           = 'X';            // dividedo em X vezes
$PRECO_VENDAS_MENSAL              = 'R$ XXX,XX';    // valor do plano
$PARCELA_VENDAS_MENSAL            = 'X';            // dividedo em X vezes


$PRECO_YYY_BASICO                 = 'R$ XXX,XX';    // valor do plano
$PARCELA_YYY_BASICO               = 'X';            // dividedo em X vezes
$PRECO_YYY_PROFISSIONAL           = 'R$ XXX,XX';    // valor do plano
$PARCELA_YYY_PROFISSIONAL         = 'X';            // dividedo em X vezes
$PRECO_YYY_PREMIUM                = 'R$ XXX,XX';    // valor do plano
$PARCELA_YYY_PREMIUM              = 'X';            // dividedo em X vezes
$PRECO_YYY_MENSAL                 = 'R$ XXX,XX';    // valor do plano
$PARCELA_YYY_MENSAL               = 'X';            // dividedo em X vezes
