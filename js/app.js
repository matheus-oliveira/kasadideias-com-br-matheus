// menu click (scrol smoothly)
const smoothLinks = document.querySelectorAll('.scroll')
for (const link of smoothLinks) {
  link.addEventListener('click', handleMenuClick)
}

function handleMenuClick(event) {
  event.preventDefault()
  const href = event.target.getAttribute('href')
  const target = document.querySelector(href)
  console.log(event)
  scrollTo(target)
}

// scrol smoothly
function scrollTo(element, offset = 0) {
  window.scroll({
    behavior: 'smooth',
    left: 0,
    top: element.offsetTop + offset
  });
}